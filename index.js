const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");

const { db } = require("./model/database");
const { readJokes, updateJokeStatusCookie } = require("./controller/joke");

const app = express();

const port = 8080;

app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(cors());

db.sequelize
  .sync()
  .then(async () => {
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });

app.get("/", readJokes);

app.post("/", updateJokeStatusCookie);

app.listen(port, () => {
  console.log(`Server's live now!`);
});
