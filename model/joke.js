module.exports = (sequelize, Sequelize) => {
  const joke = sequelize.define("joke", {
    content: {
      type: Sequelize.TEXT,
    },
    status: {
      type: Sequelize.TEXT,
    },
  });

  return joke;
};
