const Sequelize = require("sequelize");

const dbConfig = require("../config/db.config.js");
const joke = require("./joke");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,

  dialect: dbConfig.dialect,

  operatorsAliases: false,
});

const db = {};

db.Sequelize = Sequelize;

db.sequelize = sequelize;

db.joke = joke(sequelize, Sequelize);

module.exports = { db };
