const { db } = require("../model/database");
const { initDefaultCookie, setCookie } = require("./cookies");
const { cookieToObject } = require("../utils/cookie");

const joke = db.joke;

exports.updateJokeStatusCookie = async (req, res) => {
  const { id, status } = req.body;

  await setCookie(id, status, res);

  res.end();
};

const updateJokesStatus = async (newJokeStatuses) => {
  const clientStatusArray = Object.values(newJokeStatuses);
  const storedJokes = await joke.findAll({});

  const updateJobList = [];

  storedJokes.forEach((element, index) => {
    if (element.status !== clientStatusArray[index])
      updateJobList.push(
        joke.update(
          { ...element, status: clientStatusArray[index] },
          { where: { id: element.id } }
        )
      );
  });

  await Promise.all(updateJobList);
};

exports.readJokes = async (req, res) => {
  const userCookie = req.headers?.cookie;

  if (!userCookie) {
    await initDefaultCookie(res);
    return joke
      .findAll()
      .then((data) => res.send(JSON.stringify(data)).status(200))
      .catch((err) =>
        res
          .send({
            message: err,
          })
          .status(500)
      );
  }

  const jokeStatuses = cookieToObject(userCookie);

  await updateJokesStatus(jokeStatuses);

  joke
    .findAll({ where: { status: "unvoted" } })
    .then((data) => res.send(JSON.stringify(data)).status(200))
    .catch((err) =>
      res
        .send({
          message: err,
        })
        .status(500)
    );
};
