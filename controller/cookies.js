const { db } = require("../model/database");

const joke = db.joke;

exports.initDefaultCookie = async (res) => {
  const numberOfJokes = await joke.count();

  const jokeList = [];

  for (let i = 1; i <= numberOfJokes; i++) {
    jokeList.push(`joke${i}=Unvoted`);
  }

  res.setHeader("Set-Cookie", [...jokeList]);
};

exports.setCookie = async (id, status, res) => {
  res.writeHeader(200, ["Set-Cookie", `joke${id}=${status}`]);
};
